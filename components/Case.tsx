import React from 'react'
import {Text, StyleSheet, TouchableOpacity} from 'react-native'

interface Props {
    title: string,
    onPress: () => void
}

const Case = (props: Props) => {
    return (
        <TouchableOpacity style={styles.container} onPress={props.onPress}>
            <Text style={ styles.title }>
                { props.title }
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',

        borderStyle: 'solid',
        borderColor: 'grey',
        borderWidth: 1,

        backgroundColor: 'lightgrey',

        margin: 10,
        padding: 7,

        flexBasis: 150,
        flexGrow: 1,
        flexShrink: 1
    },
    title: {
        fontSize: 15,
        letterSpacing: 2,
    }
})

export default Case
