import React from 'react'
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native'

interface Props {
    image: string,
    title: string,
    onPress: () => void
}

const MenuItem = (props: Props) => {
    return (
        <TouchableOpacity style={ styles.container } onPress={props.onPress}>
            <Image
                source={{ uri: props.image }}
                style={ styles.image }
            />
            <View style={ styles.filter } />
            <Text style={ styles.title }>{ props.title }</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        height: '33.33%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        position: 'absolute',
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        letterSpacing: 5,
    },
    filter: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: 'rgba(0,0,0,0.3)',
    },
    image: {
        width: '100%',
        height: '100%',
    }
})

export default MenuItem
