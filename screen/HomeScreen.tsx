import React from 'react'
import {View, StyleSheet} from 'react-native'
import MenuItem from '../components/MenuItem'
import {Navigation} from "../types/navigation"

interface Props {
    navigation: Navigation
}

interface States {}

export default class HomeScreen extends React.Component<Props, States> {
    render () {
        const { navigation } = this.props

        return (
            <View style={ styles.container }>
                <MenuItem title="Restaurants"
                          image="https://images.unsplash.com/photo-1525648199074-cee30ba79a4a"
                          onPress={() => navigation.navigate('Restaurants')}
                />
                <MenuItem title="Hébergements"
                          image="https://images.unsplash.com/photo-1566073771259-6a8506099945"
                          onPress={() => navigation.navigate('Accommodations')}
                />
                <MenuItem title="Activités"
                          image="https://images.unsplash.com/photo-1575191291897-1717b9196436"
                          onPress={() => navigation.navigate('Activities')}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: "relative",
        height: '100%'
    }
})
