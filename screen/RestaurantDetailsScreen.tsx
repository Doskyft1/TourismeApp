import React from 'react'
import {View, StyleSheet, Text, Linking} from 'react-native'
import {Restaurant} from "../types/data-tourism"

interface Props {
    route: {
        params: {
            restaurant: Restaurant
        }
    }
}

interface States {}

export default class RestaurantDetailsScreen extends React.Component<Props, States> {
    render() {
        const { restaurant } = this.props.route.params

        return (
            <View style={ styles.container }>
                <View style={ styles.header }>
                    <Text style={ styles.title }>{restaurant['rdfs:label']['@value']}</Text>
                    <Text>{typeof restaurant['@type'] === 'string' ? restaurant['@type'] : restaurant['@type'].join(', ')}</Text>
                    <Text style={ styles.id } onPress={() => Linking.openURL(restaurant['@id'])}>
                        {restaurant['@id']}
                    </Text>
                </View>
                <View>
                    { restaurant['rdfs:comment'] &&
                        <View style={styles.box}>
                            <Text>{restaurant['rdfs:comment']['@value']}</Text>
                        </View>
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 24,
    },
    id: {
        fontStyle: 'italic',
        color: 'blue'
    },
    header: {

    },
    title: {
        fontSize: 25
    },
    box: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 10
    }
})
