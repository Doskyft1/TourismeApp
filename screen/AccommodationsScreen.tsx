import React, {Component} from 'react'
import {View, StyleSheet, Text} from 'react-native'

export default class AccommodationsScreen extends Component {
    render() {
        return (
            <View style={ styles.container }>
                <Text>AccommodationsScreen</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: "relative",
        height: '100%'
    }
})
