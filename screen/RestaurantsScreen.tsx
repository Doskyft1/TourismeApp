import React from 'react'
import {View, ActivityIndicator, StyleSheet, Text, ScrollView} from 'react-native'
import Case from '../components/Case'
import {DATA_TOURISM_RESTAURANT_URL} from '@env'
import {Restaurant} from "../types/data-tourism"

interface Props {
    navigation: any
}

interface States {
    restaurants: Array<any>,
    isLoading: boolean,
    error?: string
}

export default class RestaurantsScreen extends React.Component<Props, States> {
    constructor(props: Props) {
        super(props)

        this.state = {
            restaurants: [],
            isLoading: true,
            error: undefined
        }
    }

    componentDidMount() {
        fetch(DATA_TOURISM_RESTAURANT_URL)
            .then((response) => response.json())
            .then((json) => this.setState({restaurants: json['@graph']}))
            .catch(() => this.setState({error: 'Une erreur est survenu pendant le chargement des restaurants'}))
            .finally(() => this.setState({isLoading: false}))
    }

    renderItem = (item: Restaurant) => (
        <Case title={item['rdfs:label'] && item['rdfs:label']['@value'] ? item['rdfs:label']['@value'] : 'Erreur'}
              key={item['@id']}
              onPress={() => {
                  this.props.navigation.navigate('RestaurantDetail', {restaurant: item})
              }}
        />
    )

    render() {
        const {restaurants, isLoading, error} = this.state

        return (
            <View style={styles.container}>
                {isLoading ? <ActivityIndicator/> : (
                    error ? <Text>{error}</Text> : (
                        <ScrollView contentContainerStyle={styles.list}>
                            {restaurants.map(this.renderItem)}
                        </ScrollView>
                    )
                )}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 24,
    },
    list: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    }
})
