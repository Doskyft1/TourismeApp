import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import HomeScreen from "./screen/HomeScreen"
import ActivitiesScreen from "./screen/ActivitiesScreen"
import AccommodationsScreen from "./screen/AccommodationsScreen"
import RestaurantsScreen from "./screen/RestaurantsScreen"
import RestaurantDetailsScreen from "./screen/RestaurantDetailsScreen"

const Stack = createStackNavigator()

interface Props {}

interface States {}

export default class App extends React.Component<Props, States> {
  constructor(props: Props) {
    super(props);

    this.state = {
      data: null,
    };
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
              name="Home"
              component={ HomeScreen }
              options={{ title: 'TourismApp' }}
          />

          <Stack.Screen
              name="Restaurants"
              component={ RestaurantsScreen }
              options={{ title: 'Restaurants' }}
          />
          <Stack.Screen
              name="RestaurantDetail"
              component={ RestaurantDetailsScreen }
              options={{ title: 'Information sur le restaurant' }}
          />

          <Stack.Screen
              name="Accommodations"
              component={ AccommodationsScreen }
              options={{ title: 'Hébergements' }}
          />

          <Stack.Screen
              name="Activities"
              component={ ActivitiesScreen }
              options={{ title: 'Activités' }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}
