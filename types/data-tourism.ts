interface Language {
    "@value": string,
    "@language": 'fr' | 'en'
}

interface Typable {
    "@value": string,
    "@type": string
}

interface DataTourism {
    "@id": string,
    "@type": Array<string> | string,
}

interface Review extends DataTourism {
    "hasReviewValue": ReviewValue
}

interface ReviewValue extends DataTourism  {
    "rdfs:label": Language,
    "isCompliantWith": Array<{"@id": string}>
}

interface Address extends DataTourism {
    "schema:addressLocality"?: string,
    "schema:postalCode"?: string,
    "schema:streetAddress"?: string,
    "hasAddressCity"?: AddressCity
}

interface AddressCity extends DataTourism {
    "rdfs:label": Language,
    "insee": string,
    "isPartOfDepartment": PartOfDepartment
}

interface PartOfDepartment extends DataTourism {
    "rdfs:label": Language,
    "insee": string,
    "isPartOfRegion": PartOfRegion
}

interface PartOfRegion extends DataTourism {
    "rdfs:label": Language,
    "insee": string,
    "isPartOfCountry": PartOfCountry
}

interface PartOfCountry extends DataTourism {
    "rdfs:label": Language
}

export interface Restaurant extends DataTourism {
    "dc:identifier": string,
    "rdfs:comment"?: Language,
    "rdfs:label": Language,
    "owl:topObjectProperty": {
        "@id": string,
        "@type": Array<string> | string,
        "dc:description": Language,
        "owl:topDataProperty": Language,
        "shortDescription": Language
    },
    "COVID19OpeningPeriodsConfirmed": Typable,
    "COVID19SpecialMeasures": Language,
    "creationDate": Typable,
    "hasBeenCreatedBy": {
        "@id": string,
        "@type": Array<string> | string
        "schema:legalName": string,
    },
    "hasBeenPublishedBy": {
        "@id": string,
        "@type": Array<string> | string
        "schema:address": Address,
        "schema:legalName": string,
    },
    "hasContact": {
        "@id": string,
        "@type": Array<string> | string,
        "schema:address": Address,
        "schema:email": string,
        "schema:telephone": string,
        "foaf:homepage": string
    },
    "hasDescription": {
        "@id": string
    },
    "hasReview": Array<Review>,
    "isLocatedAt": {
        "@id": string,
        "@type": Array<string> | string,
        "schema:address": Address,
        "schema:geo": {
            "@id": string,
            "@type": Array<string> | string,
            "schema:latitude": Typable,
            "schema:longitude": Typable,
            "latlon": Typable
        },
        "altInsee": string
    },
    "lastUpdate": Typable,
    "lastUpdateDatatourisme": Typable,
    "meta:fingerprint": string,
    "meta:hasFluxIdentifier": Typable,
    "meta:hasOrganizationIdentifier": Typable,
    "meta:sourceChecksum": string
}
