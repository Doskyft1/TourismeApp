declare module '@env' {
    export const DATA_TOURISM_RESTAURANT_URL: string;
    export const DATA_TOURISM_API_KEY: string;
}
